package com.ibm.intro.exception;

/**
 * Wraps exceptions thrown by the data access layer within the service layer.
 * 
 * @author Ronald Schartmüller
 */
public class PersistenceException extends Exception {

	/** the serial version uid */
	private static final long serialVersionUID = 2L;
	
	/**
	 * Instantiates a PersistenceException with a message
	 * 
	 * @param message: the exception message
	 */
	public PersistenceException(String string) {
		super(string);
	}

	public PersistenceException(Throwable e) {
		super(e);
	}
}
