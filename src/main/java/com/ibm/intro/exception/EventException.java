package com.ibm.intro.exception;

public class EventException extends Exception {
    
	/** the serial version uid */
	private static final long serialVersionUID = 4L;
	
	/**
	 * Instantiates a EventException with a message
	 * 
	 * @param message: the exception message
	 */
	public EventException(String message) {
		super(message);
	}
}
