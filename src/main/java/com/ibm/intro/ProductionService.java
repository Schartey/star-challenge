/**
 * star-challenge
 *
 * (C) Copyright 2016 IBM Corporation
 * All rights reserved
 *
 * Creation date: 03.08.2016
 */
package com.ibm.intro;

import com.ibm.intro.dao.ProductionDao;
import com.ibm.intro.exception.DataStoreException;
import com.ibm.intro.exception.PersistenceException;
import com.ibm.intro.model.EntityState;
import com.ibm.intro.model.Production;

/**
 * @author Richard Holzeis
 */
public class ProductionService implements CRUDService<Production> {

	/*
	 * #task 2: implement me!
	 */
	@Override
	public Production create(Production object) throws PersistenceException {
		try {
			return ProductionDao.getInstance().persist(object);
		} catch (DataStoreException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public Production read(String id) {
		return ProductionDao.getInstance().load(id);
	}

	@Override
	public synchronized Production update(Production object) throws PersistenceException {
			Long ts = ProductionDao.getInstance().loadLastChangeDate(object.getId());
			if(ts == null) {
				throw new PersistenceException("object does not exist");
			}
			if(ts >= object.getChangeDate()) {
				throw new PersistenceException("object has changed");
			}
			try {
				object.setEntityState(EntityState.UPDATED);
				return ProductionDao.getInstance().persist(object);
			} catch (DataStoreException e) {
				throw new PersistenceException(e);
			}
	}

	@Override
	public void delete(String id) throws PersistenceException {
		Production p = ProductionDao.getInstance().load(id);
		if(p == null) {
			throw new PersistenceException("object does not exist");
		}
		p.setEntityState(EntityState.DELETED);
		try {
			ProductionDao.getInstance().persist(p);
		} catch (DataStoreException e) {
			throw new PersistenceException(e);
		}
	}
}
