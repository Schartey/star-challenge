/**
 * star-challenge
 *
 * (C) Copyright 2016 IBM Corporation
 * All rights reserved
 *
 * Creation date: 03.08.2016
 */
package com.ibm.intro;

import com.ibm.intro.exception.PersistenceException;
import com.ibm.intro.model.AbstractObject;

/**
 * @author Richard Holzeis
 * @param <O> the business object
 */
public interface CRUDService<O extends AbstractObject> {

	/*
	 * #task 2: define me
	 * 
	 * create, read, update, delete
	 */

	/**
	 * Creates a new object in the persistence layer.
	 * 
	 * @param object: the object to create.
	 * @return the stored object.
	 * @throws PersistenceException is thrown if the object could not be created.
	 */
	public O create(O object) throws PersistenceException;

	/**
	 * Loads a copy of an object based on the given id from the persistence layer.
	 * 
	 * @param id: the id of the object to load.
	 * @return the found object or null if not found.
	 */
	public O read(String id);

	/**
	 * Persists the object within the persistence layer based on the object id.
	 * 
	 * Note: Imo it's weird that we do the dirty write check in the service. The persistence layer seems more suitable.
	 * 
	 * @param object: the object to update.
	 * @return the updated object.
	 * @throws PersistenceException is thrown if the object the object could not be found or was changed in the meantime.
	 */
	public O update(O object) throws PersistenceException;

	/**
	 * Deletes the object within the persistence layer based on the object id.
	 * 
	 * @param id: the id of the object to delete.
	 * @throws PersistenceException is thrown if the object the object could not be found or deleted.
	 */
	public void delete(String id) throws PersistenceException;
}
