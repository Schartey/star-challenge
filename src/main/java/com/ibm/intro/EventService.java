/**
 * star-challenge
 *
 * (C) Copyright 2016 IBM Corporation
 * All rights reserved
 *
 * Creation date: 03.08.2016
 */
package com.ibm.intro;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import com.ibm.intro.exception.EventException;
import com.ibm.intro.exception.InvalidStateTransitionException;
import com.ibm.intro.exception.PersistenceException;
import com.ibm.intro.model.Event;
import com.ibm.intro.model.EventType;
import com.ibm.intro.model.Production;
import com.ibm.intro.model.ProductionType;
import com.ibm.intro.model.Status;

/**
 * @author Richard Holzeis
 */
public class EventService {
	/*
	 * #task 3: implement me
	 * 
	 */

	private Logger logger = Logger.getLogger(EventService.class.getName());
	
	private ExecutorService es;
	private ProductionService ps;

	private Map<ProductionType, StateMachine<Status>> stateMachines;

	public EventService() {
		es = Executors.newCachedThreadPool();

		ps = new ProductionService();

		StateMachine<Status> collectionStateMachine = new StateMachine<>();
		collectionStateMachine.addTransition(Status.CREATED, EventType.COLLECTED, Status.INPRODUCTION);
		collectionStateMachine.addTransition(Status.INPRODUCTION, EventType.ENTRY, Status.COMPLETED);

		StateMachine<Status> linehaulStateMachine = new StateMachine<>();
		linehaulStateMachine.addTransition(Status.CREATED, EventType.EXIT, Status.INPRODUCTION);
		linehaulStateMachine.addTransition(Status.INPRODUCTION, EventType.ENTRY, Status.COMPLETED);

		StateMachine<Status> deliveryStateMachine = new StateMachine<>();
		deliveryStateMachine.addTransition(Status.CREATED, EventType.EXIT, Status.INPRODUCTION);
		deliveryStateMachine.addTransition(Status.INPRODUCTION, EventType.DELIVERED, Status.COMPLETED);

		stateMachines = new HashMap<>();
		stateMachines.put(ProductionType.COLLECTION, collectionStateMachine);
		stateMachines.put(ProductionType.LINEHAUL, linehaulStateMachine);
		stateMachines.put(ProductionType.DELIVERY, deliveryStateMachine);
	}

	public void execute(Event event) {
		this.es.submit(() -> {
			try {
				process(event);
			} catch(EventException e) {
				logger.warning(e.getMessage());
			}
		});
	}

	public void process(Event event) throws EventException {
		if(event == null) {
			throw new EventException("invalid event");
		}
		Production p = ps.read(event.getProductionId());
		if(p == null) {
			throw new EventException("invalid production in event");
		}
		StateMachine<Status> sm = this.stateMachines.get(p.getProductionType());
		if(sm == null) {
			throw new EventException(String.format("invalid production type: %s", event.getEventType()));
		}
		try {
			p.setStatus(sm.transition(p.getStatus(), event.getEventType()));
		} catch(InvalidStateTransitionException e) {
			throw new EventException(String.format("invalid state transition from %s to %s for production type %s", 
				p.getStatus(), event.getEventType(), p.getProductionType()));
		}

		try {
			ps.update(p);
		} catch (PersistenceException e) {
			throw new EventException(String.format("production could not be updated: %s", e.getMessage()));
		}
	}
}
