package com.ibm.intro;

import java.util.HashMap;
import java.util.Map;

import com.ibm.intro.exception.InvalidStateTransitionException;
import com.ibm.intro.model.EventType;

public class StateMachine<T> {
	private Map<T, Map<EventType, T>> transitions;

    public StateMachine() {
        this.transitions = new HashMap<>();
    }

    public synchronized void addTransition(T from, EventType eventType, T to) {
        if(!this.transitions.containsKey(from)) {
            this.transitions.put(from, new HashMap<>());
        }
        this.transitions.get(from).put(eventType, to);
    }

    public T transition(T from, EventType eventType) throws InvalidStateTransitionException {
        if(this.transitions.containsKey(from)) {
            if(this.transitions.get(from).containsKey(eventType)) {
                return this.transitions.get(from).get(eventType);
            }
        }
        throw new InvalidStateTransitionException();
    }
}