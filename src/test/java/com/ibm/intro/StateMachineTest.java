package com.ibm.intro;

import com.ibm.intro.exception.InvalidStateTransitionException;
import com.ibm.intro.model.EventType;
import com.ibm.intro.model.Status;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class StateMachineTest {

    @Rule
	public final ExpectedException exception = ExpectedException.none();

	@Test
	public void testTransition() throws InvalidStateTransitionException {
		StateMachine<Status> sm = new StateMachine<Status>();
        sm.addTransition(Status.CREATED, EventType.COLLECTED, Status.INPRODUCTION);

        exception.expect(InvalidStateTransitionException.class);
        sm.transition(Status.INPRODUCTION, EventType.DELIVERED);

        Assert.assertEquals(Status.INPRODUCTION, sm.transition(Status.CREATED, EventType.COLLECTED));
    }
}
