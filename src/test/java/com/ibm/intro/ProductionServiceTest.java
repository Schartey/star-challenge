/**
 * star-challenge
 *
 * (C) Copyright 2016 IBM Corporation
 * All rights reserved
 *
 * Creation date: 03.08.2016
 */
package com.ibm.intro;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeoutException;

import com.ibm.intro.exception.PersistenceException;
import com.ibm.intro.model.Production;
import com.ibm.intro.model.ProductionType;
import com.ibm.intro.model.Status;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author Richard Holzeis
 */
public class ProductionServiceTest {

	/*
	 * #task 2: implement me!
	 */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Test
	public void testOptimisticLockingForUpdate() throws PersistenceException, InterruptedException, ExecutionException, TimeoutException {
		final Production production = new Production(ProductionType.COLLECTION, 1L);

		ProductionService productionService = new ProductionService();
		Production p = productionService.create(production);

		final String productionId = p.getId();

		ExecutorService executor = Executors.newFixedThreadPool(1);
		// lock production with a different thread.
		FutureTask<Production> update = new FutureTask<Production>(new Callable<Production>() {

			@Override
			public Production call() throws Exception {
				Production tp = productionService.read(productionId);
				tp.setStatus(Status.INPRODUCTION);
				return productionService.update(tp);
			}
		});

		executor.execute(update);
		
		exception.expect(PersistenceException.class);
		p.setStatus(Status.INPRODUCTION);
		productionService.update(p);

		p = productionService.read(productionId);
		p.setStatus(Status.COMPLETED);
		p = productionService.update(p);
		Assert.assertEquals(p.getStatus(), Status.COMPLETED);
	}
}
