/**
 * star-challenge
 *
 * (C) Copyright 2016 IBM Corporation
 * All rights reserved
 *
 * Creation date: 03.08.2016
 */
package com.ibm.intro;

import com.ibm.intro.exception.EventException;
import com.ibm.intro.exception.PersistenceException;
import com.ibm.intro.model.Event;
import com.ibm.intro.model.EventType;
import com.ibm.intro.model.Production;
import com.ibm.intro.model.ProductionType;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author Richard Holzeis
 */
public class EventServiceTest {
	
	/*
	 * #task 3: implement me! 
	 */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Test
	public void testInvalidEvent() throws EventException {
		EventService es = new EventService();

		exception.expect(EventException.class);
		es.process(null);

		exception.expect(EventException.class);
		es.process(new Event(EventType.COLLECTED, null));
		exception.expect(EventException.class);
		es.process(new Event(EventType.COLLECTED, "non-existent"));
	}

	@Test
	public void testTransition() throws EventException, PersistenceException {
		ProductionService ps = new ProductionService();
		Production collection = ps.create(new Production(ProductionType.COLLECTION, 1L));
		Production delivery = ps.create(new Production(ProductionType.DELIVERY, 2L));
		Production linehaul = ps.create(new Production(ProductionType.LINEHAUL, 3L));
		EventService es = new EventService();

		// Check invalid state transitions
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, collection.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.ENTRY, collection.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.EXIT, collection.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, collection.getId()));

		// Check invalid state transitions
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, delivery.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.ENTRY, delivery.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.COLLECTED, delivery.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, delivery.getId()));

		// Check invalid state transitions
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, linehaul.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.ENTRY, linehaul.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.COLLECTED, linehaul.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, linehaul.getId()));

		// Check first valid state transitions
		es.process(new Event(EventType.COLLECTED, collection.getId()));
		es.process(new Event(EventType.EXIT, delivery.getId()));
		es.process(new Event(EventType.EXIT, linehaul.getId()));

		
		// Check invalid state transitions
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, collection.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.COLLECTED, collection.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.EXIT, collection.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, collection.getId()));

		// Check invalid state transitions
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, delivery.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.ENTRY, delivery.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.COLLECTED, delivery.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.EXIT, delivery.getId()));

		// Check invalid state transitions
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, linehaul.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.ENTRY, linehaul.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.COLLECTED, linehaul.getId()));
		exception.expect(EventException.class);
		es.process(new Event(EventType.DELIVERED, linehaul.getId()));

		// Check first valid state transitions
		es.process(new Event(EventType.ENTRY, collection.getId()));
		es.process(new Event(EventType.DELIVERED, delivery.getId()));
		es.process(new Event(EventType.ENTRY, linehaul.getId()));		
	}
}
